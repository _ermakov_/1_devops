FROM nginx:1-alpine
COPY docker/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
EXPOSE 8080